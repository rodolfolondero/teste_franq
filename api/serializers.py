from rest_framework import serializers
from pessoa.models import Pessoa
from garagem.models import Garagem, Veiculo


class VeiculoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Veiculo
        fields = '__all__'

class GaragemSerializer(serializers.ModelSerializer):
    class Meta:
        model = Garagem
        fields = '__all__'

class PessoaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pessoa
        fields = '__all__'
