from rest_framework import viewsets, generics
from garagem.models import Garagem, Veiculo
from pessoa.models import Pessoa
from .serializers import GaragemSerializer, PessoaSerializer, VeiculoSerializer


class VeiculoList(viewsets.ModelViewSet):
    queryset = Veiculo.objects.all().order_by('modelo')
    serializer_class = VeiculoSerializer


class GaragemList(viewsets.ModelViewSet):
    queryset = Garagem.objects.all().order_by('pessoa')
    serializer_class = GaragemSerializer


class PessoaList(viewsets.ModelViewSet):
    queryset = Pessoa.objects.all().order_by('nome')
    serializer_class = PessoaSerializer

class PessoaComVeiculosList(viewsets.ModelViewSet):
    serializer_class = GaragemSerializer

    def get_queryset(self):
        garagem = Garagem.objects.filter(veiculo__isnull=False)

        pessoas = []
        for g in garagem:
            if g.pessoa in Pessoa.objects.all():
                pessoas.append(g)

        print(garagem)
        return pessoas

class PessoaSemVeiculosList(viewsets.ModelViewSet):
    serializer_class = GaragemSerializer

    def get_queryset(self):    
        garagem = Garagem.objects.filter(veiculo__isnull=True)  
        pessoas_all = Pessoa.objects.all()
        
        ids = []
        for p in pessoas_all:
            ids.append(p.id)

        pessoas = []
        for g in garagem:
            if g.pessoa.id in ids:
                pessoas.append(g)

        return pessoas

class GerarGaragens(viewsets.ModelViewSet):
    serializer_class = GaragemSerializer

    def get_queryset(self):
        garagens = Garagem.objects.all()

        pessoas_in_garagens = []
        for g in garagens:
            pessoas_in_garagens.append(g.pessoa)

        garagens_criadas = []
        for pessoa in Pessoa.objects.all():
            if pessoa not in pessoas_in_garagens:
                garagem = Garagem(pessoa=pessoa, telefone=pessoa.telefone, email=pessoa.email)
                garagem.save()
                garagens_criadas.append(garagem)

        return garagens_criadas

                
        
        