from django.contrib import admin
from django.urls import path, include
from django.views.generic.base import TemplateView
from django.conf import settings
from rest_framework import routers
from api.views import GaragemList, GerarGaragens, PessoaComVeiculosList, PessoaList, PessoaSemVeiculosList, VeiculoList

from rest_framework import routers

admin.autodiscover()
router = routers.DefaultRouter()
router.register("garagens", GaragemList)
router.register("pessoas", PessoaList)
router.register("veiculos", VeiculoList)
router.register("pessoas-com-veiculos", PessoaComVeiculosList, basename="pessoa-com-veiculos")
router.register("pessoas-sem-veiculos", PessoaSemVeiculosList, basename="pessoa-sem-veiculos")
router.register("gerar-garagens", GerarGaragens, basename="gerar-garagens")

urlpatterns = [
    path('admin/', admin.site.urls),
    path('conta/', include("django.contrib.auth.urls")),
    path('', TemplateView.as_view(template_name="index.html"), name="index"),
    path('api/', include(router.urls)),
]

for app_name in settings.LOCAL_APPS:
    urlpatterns += [
        path(f"{app_name}/", include(f"{app_name}.urls", namespace=str(app_name))),
    ]