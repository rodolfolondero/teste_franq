from garagem.models import Garagem, Veiculo
from django.contrib import admin

@admin.register(Veiculo)
class AdminVeiculo(admin.ModelAdmin):
    model = Veiculo
    list_display = ('cor', 'ano', 'modelo', 'tipo')

@admin.register(Garagem)
class AdminGaragem(admin.ModelAdmin):
    model = Garagem
    list_display = ('pessoa', 'telefone', 'email')