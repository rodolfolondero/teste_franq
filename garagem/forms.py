from django import forms
from .models import Garagem, Veiculo

class GaragemModelForm(forms.ModelForm):
    class Meta:
        model = Garagem
        fields = "__all__"


class VeiculoModelForm(forms.ModelForm):
    class Meta:
        model = Veiculo
        fields = "__all__"