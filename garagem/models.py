from django.db import models
from django.contrib.auth import get_user_model


class Veiculo(models.Model):
    tipo_veiculo = [
        (0, "Carro"),
        (1, "Moto")
    ]
    
    cor = models.CharField("Cor", max_length=50, blank=True, null=True)
    ano = models.IntegerField("Ano", blank=True, null=True)
    modelo = models.CharField("Modelo", max_length=50, blank=True, null=True)
    tipo = models.IntegerField("Tipo", choices=tipo_veiculo, default=0)

    class Meta:
        verbose_name = "Veiculo"
        verbose_name_plural = "Veiculos"

    def __str__(self):
        if self.tipo == 0:
            result = f"Carro, {self.ano}, {self.cor}"
        else:
            result = f"Moto, {self.ano}, {self.modelo}"

        return result


class Garagem(models.Model):
    pessoa = models.ForeignKey(get_user_model(), on_delete=models.PROTECT)
    veiculo = models.ForeignKey(Veiculo, blank=True, null=True, on_delete=models.PROTECT)
    telefone = models.CharField('Telefone', max_length=15)
    email = models.EmailField("E-mail", max_length=100)

    class Meta:
        verbose_name = "Garagem"
        verbose_name_plural = "Garagens"
        constraints = [
            models.UniqueConstraint(fields=['pessoa', 'veiculo'], name='Veiculo único para pessoa')
        ]
        ordering = ['pessoa']

    def __str__(self):
        return f"{self.pessoa} - {self.veiculo} - {self.telefone} - {self.email}"