from garagem import views_veiculo
from django.urls import path

from . import views, views_veiculo

app_name = 'garagem'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:veiculo_id>/vincular', views.create, name='create'),
    path('<int:veiculo_id>/desvincular', views.remove, name='desvincular'),    
    path('<int:id>/update', views.update, name='update'),
    path('<int:id>/delete', views.delete, name='delete'),

    path('gerar', views.auto_create, name='auto_create'),

    path('veiculos', views_veiculo.index, name='index_veiculos'),
    path('veiculos/cadastrar', views_veiculo.create, name='create_veiculo'),
    path('veiculos/<int:id>/update', views_veiculo.update, name='update_veiculo'),
    path('veiculos/<int:id>/delete', views_veiculo.delete, name='delete_veiculo'),
]
