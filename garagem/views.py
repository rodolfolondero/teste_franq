from garagem.models import Veiculo
import os 
from django.http import request
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib import messages
from rest_framework import viewsets

from pessoa.models import Pessoa
from .forms import GaragemModelForm, Garagem
# from .serializer import PessoaSerializer

app_name = 'garagem'

pages = {
    'index.html': os.path.join(app_name, "index.html"),
    f'{app_name}_form.html': os.path.join(app_name, f"{app_name}_form.html"),
    f'{app_name}_delete.html': os.path.join(app_name, f"{app_name}_delete.html"),
    f'{app_name}_list_veiculos.html': os.path.join(app_name, f"{app_name}_list_veiculos.html")
}

context = {'titulo': 'Garagens'}

def index(request):
    
    if (request.user.is_staff):
        garagens = Garagem.objects.all()
        context['garagens'] = garagens        
        return render(request, pages['index.html'], context)
    else:
        garagem = Garagem.objects.filter(pessoa=request.user.id)
        
        if not garagem.exists():
            garagem = Garagem(pessoa=request.user, telefone=request.user.telefone, email=request.user.email)
            garagem.save()

        garagens = Garagem.objects.filter(pessoa=request.user.id)

        # Veiculos vinculados
        veiculos_vinculados = []
        for garagem in garagens:
            veiculos_vinculados.append(garagem.veiculo)
        
        for v in veiculos_vinculados:
            if not v:
                veiculos_vinculados.remove(v)

        print(request.user)
        print(veiculos_vinculados)
        # Veiculos não vinculados
        veiculos_nao_vinculados = []
        veiculos = Veiculo.objects.all()
        for veiculo in veiculos:
            if veiculo not in veiculos_vinculados:
                veiculos_nao_vinculados.append(veiculo)

        context['garagem'] = garagem        
        context['veiculos_vinculados'] = veiculos_vinculados
        context['veiculos_nao_vinculados'] = veiculos_nao_vinculados


        return render(request, pages[f'{app_name}_list_veiculos.html'], context)


def create(request, veiculo_id):

    veiculo = Veiculo.objects.get(id=veiculo_id)

    garagem = Garagem(pessoa=request.user, veiculo=veiculo, telefone=request.user.telefone, email=request.user.email)
    garagem.save()

    return redirect('garagem:index')


def remove(request, veiculo_id):
    
    garagem = Garagem.objects.filter(veiculo=veiculo_id, pessoa=request.user.id)
    garagem.delete()
    return redirect('garagem:index')


def update(request, id):
    garagem = get_object_or_404(Garagem, id=id)

    if request.user.is_authenticated:
        form = GaragemModelForm(request.POST or None, instance=garagem)

    context['form'] = form


    if str(request.method) == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, 'Garagem atualizada com sucesso.')

            return redirect('garagem:index')
        else:
            messages.error(request, 'Não foi possível atualizar o cadastro.')

    return render(request, pages[f'{app_name}_form.html'], context)


def delete(request, id):
    garagem = get_object_or_404(Garagem, id=id)
    context['garagem'] = garagem

    if str(request.method) == 'POST':
        garagem.delete()
        return redirect('garagem:index')

    return render(request, pages[f'{app_name}_delete.html'], context)
    

def auto_create(request):

    pessoas = Pessoa.objects.all()

    for p in pessoas:

        exist = Garagem.objects.filter(pessoa=p.id).exists()
        if not exist:
            garagem = Garagem(pessoa=p, telefone=p.telefone, email=p.email)
            garagem.save()
    
    messages.success(request, "Garagens criadas com sucesso.")
    return redirect('garagem:index')
        