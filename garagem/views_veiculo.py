import os 
from django.http import request
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib import messages
from rest_framework import viewsets

from .models import Veiculo
from .forms import *
from .apps import GaragemConfig
# from .serializer import PessoaSerializer

app_name = 'veiculo'

pages = {
    'index.html': os.path.join(app_name, "index.html"),
    f'{app_name}_form.html': os.path.join(app_name, f"{app_name}_form.html"),
    f'{app_name}_delete.html': os.path.join(app_name, f"{app_name}_delete.html")
}

context = {'titulo': 'Veículos'}

def index(request):
    
    if (request.user.is_staff):
        veiculos = Veiculo.objects.all()
        
        context['veiculos'] = veiculos        
        
        return render(request, pages['index.html'], context)

    return redirect('index')

def create(request):

    if request.user.is_staff:
        form = VeiculoModelForm(request.POST or None)
        context['form'] = form

    if str(request.method) == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, 'Veículo cadastrado com sucesso.')
            return redirect('garagem:index_veiculos')
        else:
            messages.error(request, 'Não foi possível cadastrar o veículo.')

    
    return render(request, pages[f'{app_name}_form.html'], context)

def update(request, id):
    if (request.user.is_staff):
        veiculo = get_object_or_404(Veiculo, id=id)

        # if not request.user.is_staff:
        form = VeiculoModelForm(request.POST or None, instance=veiculo)
        context['form'] = form

        if str(request.method) == "POST":
            if form.is_valid():
                form.save()
                messages.success(request, 'Veículo atualizado com sucesso.')

                return redirect('garagem:index_veiculos')
            else:
                messages.error(request, 'Não foi possível atualizar o veículo.')

        return render(request, pages[f'{app_name}_form.html'], context)

    return redirect('index')

def delete(request, id):
    
    if (request.user.is_staff):    
        veiculo = get_object_or_404(Veiculo, id=id)
        context['veiculo'] = veiculo

        if str(request.method) == 'POST':
            try:
                veiculo.delete()
                return redirect('garagem:index_veiculos')
            except:
                messages.warning(request, "Não é possível excluir o veículo, pois ele possui garagens vinculadas.")
                return redirect('garagem:index_veiculos')

        return render(request, pages[f'{app_name}_delete.html'], context)
    return redirect('index')