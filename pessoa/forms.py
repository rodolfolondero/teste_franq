from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django import forms
from .models import Pessoa


class PessoaModelFormConsumidor(forms.ModelForm):
    class Meta:
        model = Pessoa
        fields = ['nome', 'telefone', 'email']


class PessoaModelFormAdmin(forms.ModelForm):
    class Meta:
        model = Pessoa
        fields = ['nome', 'telefone', 'email', 'is_staff']



class PessoaCreateForm(UserCreationForm):
    class Meta:
        model = Pessoa
        fields = ('nome', 'telefone', 'username', 'is_staff')
        labels = {'username': 'Username/Email' }

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password1'])
        user.email = self.cleaned_data['username']

        if commit:
            user.save()

        return user

class PessoaChangeForm(UserChangeForm):
    class Meta:
        models = Pessoa
        fields = ('nome', 'telefone', 'is_staff')
