from django.urls import path

from . import views

app_name = 'pessoa'

urlpatterns = [
    path('', views.index_pessoa, name='index'),
    path('cadastrar', views.create_pessoa, name='create'),
    path('<int:id>/update', views.update_pessoa, name='update'),
    path('<int:id>/delete', views.delete_pessoa, name='delete')
]
