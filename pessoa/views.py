import os 
from django.http import request
from django.shortcuts import redirect, render, get_object_or_404
from django.contrib import messages
from rest_framework import viewsets

from .models import Pessoa
from .forms import PessoaChangeForm, PessoaCreateForm, PessoaModelFormAdmin, PessoaModelFormConsumidor
from .apps import PessoaConfig

app_name = "pessoa"

pages = {
    'index.html': os.path.join(app_name, "index.html"),
    f'{app_name}_form.html': os.path.join(app_name, f"{app_name}_form.html"),
    f'{app_name}_delete.html': os.path.join(app_name, f"{app_name}_delete.html")
}


context = {'titulo': 'Pessoas'}

def index_pessoa(request):
    
    if (request.user.is_staff):
        pessoas = Pessoa.objects.all()

        context['pessoas'] = pessoas
        
        return render(request, pages['index.html'], context)

    return redirect('index')

def create_pessoa(request):

    if request.user.is_staff:
        form = PessoaCreateForm(request.POST or None)
    else:
        form = PessoaModelFormConsumidor(request.POST or None)
    
    context['form'] = form
    if str(request.method) == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, 'Pessoa cadastrada com sucesso.')
            return redirect('pessoa:index')
        else:
            messages.error(request, 'Não foi possível realizar o cadastro.')

    return render(request, pages[f'{app_name}_form.html'], context)

def update_pessoa(request, id):
    pessoa = get_object_or_404(Pessoa, id=id)

    if not request.user.is_staff:
        form = PessoaModelFormConsumidor(request.POST or None, instance=pessoa)
    else:
        form = PessoaModelFormAdmin(request.POST or None, instance=pessoa)

    context['form'] = form

    if str(request.method) == "POST":
        if form.is_valid():
            form.save()
            messages.success(request, 'Pessoa atualizada com sucesso.')

            return redirect('pessoa:index')
        else:
            messages.error(request, 'Não foi possível atualizar o cadastro.')

    return render(request, pages[f'{app_name}_form.html'], context)


def delete_pessoa(request, id):
    pessoa = get_object_or_404(Pessoa, id=id)
    context['pessoa'] = pessoa

    if str(request.method) == 'POST':
        try:
            pessoa.delete()
            return redirect('pessoa:index')
        except:
            messages.error(request, "Não é possível excluir a pessoa, pois ela possui garagens vinculadas.")
            return redirect(pages['index.html'], context)

    return render(request, pages[f'{app_name}_delete.html'], context)