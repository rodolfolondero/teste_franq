# Resumo

## **Escopos**

### Administrador/Superusuário

    - Mantém cadastros de novos usuários
    - Mantém cadastro de veículos
    - Criam uma garagem para cada usuário

### Consumidores
    - Alteram seus cadastros, com exceção de senha.
    - Vinculam/desvinculam veículos de sua garagem

## **Observações**

- O cadastro de Pessoa foi implementado estendendo Classe User. 

- Para acesso às API's
    ```
    http://localhost:8000/api/
    ```